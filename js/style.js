document.addEventListener("DOMContentLoaded", function() {
  let tabs = document.querySelectorAll(".tabs-title");
  let tabContents = document.querySelectorAll(".tabs-content li");

  function switchTab(tabIndex) {
    tabContents.forEach(function(content) {
      content.style.display = "none";
    });

    tabs.forEach(function(tab) {
      tab.classList.remove("active");
    });

    tabs[tabIndex].classList.add("active");
    tabContents[tabIndex].style.display = "block";
  }

  tabs.forEach(function(tab, index) {
    tab.addEventListener("click", function() {
      switchTab(index);
    });
  });

  switchTab(0);
});
document.addEventListener("DOMContentLoaded", function() {
  let themeToggle = document.getElementById("theme-toggle");
  let theme = localStorage.getItem("theme");

  if (theme) {
    document.body.classList.add(theme);
  }

  themeToggle.addEventListener("click", function() {
    if (document.body.classList.contains("light-theme")) {
      document.body.classList.remove("light-theme");
      document.body.classList.add("dark-theme");
      localStorage.setItem("theme", "dark-theme");
    } else {
      document.body.classList.remove("dark-theme");
      document.body.classList.add("light-theme");
      localStorage.setItem("theme", "light-theme");
    }
  });
});